"""
Read in .info file and extract range of cds and peptide seq
extract cds seq from .seqdb.fa
Extract cds from bed and fasta

c10129_g1_i1	350	c10129_g1_i1:154:306:1:+	154	306	+		MILVLARCACLLIXXHQSRVGSFTSGRKAGTLKETRIDTDPHPMEGWGGD	51

0-seqname - identifier for a sequence
1-length
2-cds name
3-start - Sequence numbering starts at 1
4-end
5-strand - Defines the strand - either '+' or '-'
.
-2-peptide seq
"""
import os,sys
from Bio import SeqIO

def ReverseComplement(seq):
    seq_dict = {'A':'T','a':'t','T':'A','t':'a','G':'C','g':'c','C':'G','c':'g','N':'N','n':'n'}
    return "".join([seq_dict[base] for base in reversed(seq)])

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python framedp_process_output.py inDIR outDIR"
		sys.exit(0)
	
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	for i in os.listdir(inDIR):
		if i[-9:] != ".seqdb.fa": continue
		print "Reading in the fasta file",i
		handle = open(inDIR+i,"r")
		seqDICT = {} #key is seqid, value is seq
		for record in SeqIO.parse(handle,"fasta"):
			seqDICT[str(record.id)] = str(record.seq)
		handle.close
		
		summary = i.replace(".seqdb.fa",".summary")
		print "Reading the summary file",summary
		infile = open(inDIR+summary,"r")
		outfile1 = open(outDIR+i.split(".")[0]+"_pep.fa","w") #pep sequence
		outfile2 = open(outDIR+i.split(".")[0]+"_cds.fa","w") #cds sequence
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) != 8 and len(spls) != 9:
				print "Error in parsing .summary file",summary
				print line
				sys.exit(0)
			
			seqid,cdsid,start,end = spls[0],spls[2],int(spls[3]),int(spls[4])
			strand,pepseq = spls[5],spls[-2]
			seq = seqDICT[seqid]
			cdsseq = seq[start-1:end]
			cdsid = cdsid.replace(":","_") #c100001_g1_i1:95:193:2:+
			cdsid = cdsid.replace("+","plus")
			cdsid = cdsid.replace("-","minus")
			#print seqid,cdsid,start,end,strand,pepseq,cdsseq
			if strand == "-":
				cdsseq = ReverseComplement(cdsseq)
			'''
			if end > len(seq):
				print i,cdsid
				print seq
				print cdsseq
				print pepseq
				print'''
			outfile1.write(">"+cdsid+"\n"+pepseq+"\n")
			outfile2.write(">"+cdsid+"\n"+cdsseq+"\n")
		infile.close()
		outfile1.close()
		outfile2.close()
	
