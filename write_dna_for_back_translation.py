"""
Input: 
- dna fasta file, concatenate all individual files, and fix direction
can be full transcript including UTR for PAL2NAL
- extracted clades from trees after monophyly masking

Output: dna fasta files with same seq and matching pep seq name for PAL2NAL
"""

import sys,os,newick3
from Bio import SeqIO
from Bio.Seq import Seq

FILE_ENDING = ".cary"

def get_taxon_code(seqid):
	return seqid[:4] #original seq, no reverse complimentary etc.
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
if __name__ =="__main__":
	if len(sys.argv) != 4:
		print "usage: write_dna_for_back_translation.py dna cladeDIR dnafastaDIR"
		sys.exit()
	
	mmDIR = sys.argv[2]+"/"
	cdsDIR = sys.argv[3]+"/"
	
	#get the dna seqs into a dictionary
	print "Reading the fasta file"
	handle = open(sys.argv[1],"rU")
	dnaDICT = {} #key is taxon code, value is seq id
	for record in SeqIO.parse(handle,"fasta"):
		seqid = str(record.id)
		seq = str(record.seq)
		taxon_code = get_taxon_code(seqid)
		if taxon_code not in dnaDICT:
			dnaDICT[taxon_code] = {} #key is seq id, value is seq
			print "Reading new taxon",taxon_code
		dnaDICT[taxon_code][seqid] = seq
	handle.close()
	
	print "write out unaligned dna fasta files according to pep alignments"
	#each dna can have multiple pep seq after translation
	for mm in os.listdir(mmDIR):
		if mm[-len(FILE_ENDING):] != FILE_ENDING:
			continue
		with open(mmDIR+mm,"r")	as infile:
			intree = newick3.parse(infile.readline())
		pepnames = get_front_labels(intree)
		outfile = open(cdsDIR+mm+".cds","w")
		for pepname in pepnames:
			pepname = pepname.replace("-","_")
			if pepname[:3] == "ILU": #six new seqs ILU3@comp78647_c0_seq1_2_1396
				#splt = pepname.split("_")
				#dnaname = splt[0]+"_"+splt[1]+"_"+splt[2]
				dnaname = pepname
				dnaseq = dnaDICT[get_taxon_code(dnaname)][dnaname]
			elif pepname[:4] == "BVGI": #Beta
				print pepname
				dnaname = pepname.split("_")[0]
				dnaseq = dnaDICT[get_taxon_code(dnaname)][dnaname]
				if pepname[-2:] == "__":
					print pepname
					seq_obj = Seq(dnaseq)
					dnaseq = str(seq_obj.reverse_complement())
			elif pepname[4] == "@": #rest of the ingroup, 1KP
				dnaname = pepname
				dnaseq = dnaDICT[get_taxon_code(dnaname)][dnaname]
			"""
			else: #outgroup, match by peptide number
				#looks like Cclementina__Ciclev10000445m__20789105_peptide__37895454
				pepnumber = pepname.split("__")[-2]
				for i in dnaDICT[get_taxon_code(pepname)]:
					if pepnumber in i:
						dnaname = i
						dnaseq = dnaDICT[get_taxon_code(pepname)][i]
						"""
			outfile.write(">"+pepname+"\n"+dnaseq+"\n")
		outfile.close()
