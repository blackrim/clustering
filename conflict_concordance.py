import os,sys
import phylo3,newick3
import tree_utils

def nested(front,back,old_front,old_back):
	"""check whether the first bipartition is nested within the second"""
	if front <= old_front and back <= old_back:
		return True
	else: return False
		
def tree_deconstructor(root,support_cutoff=0):
	biparts = [] # list of deconstructed bipartitions
	# each bipart looks like: [front_name_set,back_name_set]
	for node in root.iternodes(order=0): # preorder, root to tip
		if node.istip or node == root: continue
		if support_cutoff > 0 and float(node.label) < float(support_cutoff):
			continue	
		front = set(tree_utils.get_front_names(node))
		back = set(tree_utils.get_back_names(node,root))	
		# same taxon id found in both fr and bk
		if len(front & back) > 0: 
			last,current = node,node.parent
			current_back = set()
			while current != None:
				other = last.get_sisters()[0]
				"""  last       other
				         \      /
				          current    """
				other_front = set(tree_utils.get_front_names(other))
				# compare to the original front
				if len(other_front & front) == 0: # current has no dup
					current_back |= other_front # add to the back
				last = current
				current = current.parent
			back = current_back
		if len(back) == 0: continue
		print front,back
		
		# compare to existing biparts and output
		to_add = True
		for f,b in biparts:
			if front == f and back == b:
				to_add = False
		if to_add: biparts.append([front,back])
	for i in biparts: print i
	return biparts

def map_biparts(all_biparts,sptree):
	# get all the bipartitions from the species tree
	sp_biparts = tree_deconstructor(root,support_cutoff=0,unique=False)
	for f,b in sp_biparts:
		sup,conf = 0,0 # count the number of genes that support vs. conflict
		for biparts in all_biparts.values():
			for f,b in biparts
				if nested(front,back,f,b): sup += 1
		
		

"""
def dup():
	# check for duplication
	front_taxonids = tree_utils.get_front_names(node)
	num_front_tips = len(front_taxonids)
	if num_front_tips == 2:
		dup = False
	elif num_front_tips == 3:
		if len(set(front_taxonids)) < 3:
			dup = False
	else: # more than 3 tips in front
		assert len(node.children) == 2,"non bifurcating node found"
		child1,child2 = node.children
		if child1.istip or child2.istip:
			dup = False
		else:
			names1 = set(tree_utils.get_front_names(child1))
			names2 = set(tree_utils.get_front_names(child2))
			dup = True if len(names1 & names2) >= 2 else False"""

def main(incladeDIR,sptree,bootstrap_filter):
	if incladeDIR[-1] != "/":incladeDIR += "/"
	bootstrap_filter = float(bootstrap_filter)
	all_biparts = {} # key is the tree id, value is the bipart list
	for i in os.listdir(incladeDIR):
		print i
		with open(incladeDIR+i,"r") as infile:
			intree = newick3.parse(infile.readline())
		all_biparts[i] = tree_deconstructor(intree,bootstrap_filter)
	print len(all_biparts),"files read"
	map_biparts(all_biparts,sptree)
	
if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python conflict_concordance.py incladeDIR rooted_ingroup_tre bootstrap_filter"
		sys.exit(0)
	
	incladeDIR,sptree,bootstrap_filter = sys.argv[1:]
	main(incladeDIR,sptree,bootstrap_filter)
	
