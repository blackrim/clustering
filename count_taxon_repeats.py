import os,sys
import tree_utils
import phylo3,newick3

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python count_taxon_repeats.py treeDIR"
		sys.exit(0)
		
	DIR = sys.argv[1]
	if DIR[-1] != "/": DIR += "/"
	DICT = {} # key is name, value is # times it occured among all trees
	tree_count = 0 # keep track of how many trees processed
	for i in os.listdir(DIR):
		with open(DIR+i,"r") as infile:
			tree = newick3.parse(infile.readline())
		tree_count += 1
		names = tree_utils.get_front_names(tree)
		for name in names:
			if name not in DICT:
				DICT[name] = 1
			else: DICT[name] += 1
	print tree_count,"trees processed"
	print DICT
			
