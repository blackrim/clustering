import sys,os,newick3
from Bio import SeqIO

MIN_INGROUP_TAXA = 8
ITERATIONS = 3


#all ingroup datasets have "@" after the four-character taxon name code
#will skip fasta file if the number of uniq taxon codes are less than this
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

if __name__ =="__main__":
	if len(sys.argv) != 4:
		print "usage: dna_mcl_sate_wrapper.py alnDIR treDIR outDIR"
		sys.exit()
	
	alnDIR = sys.argv[1]+"/" #cc9-2.final.fa.aln
	treDIR = sys.argv[2]+"/" #RAxML_bestTree.cc9-2.final.fa.aln-cln.phy
	outDIR = sys.argv[3]+"/" #cc9-2.satein.aln,cc9-2.satein.tre
	
	for i in os.listdir(treDIR):
		if "best" not in i: continue
		ccID = i.split(".")[1]
		done = os.listdir(outDIR)
		if ccID+".sate.aln" in done: continue
		
		#Reading the fasta file
		alnfile = alnDIR+ccID+".final.fa.aln"
		seqDICT = {} #key is seqid, value is seq
		with open(alnfile,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				seqDICT[str(record.id)] = str(record.seq)
				
		#reading the tree
		trefile = treDIR+i
		with open(trefile, "r") as infile:
			intree = newick3.parse(infile.readline()) #read the tree
			
		#write the input alignment for sate with name switched
		#sate has different ways of handling special chrs
		#when reading trees vs. aligment
		alnfile_satein = outDIR+ccID+".satein.aln"
		nameDICT = {} #a two-way dictionary
		#if key is shortened seqid (count), value is full seqid
		#also if key is full seqid, value will be shortened seqid
		count = 1
		if len(set(get_front_names(intree))) >= MIN_INGROUP_TAXA:
			with open(alnfile_satein,"w") as outfile:
				labels = get_front_labels(intree)
				for label in labels:
					shortid = str(count)
					nameDICT[shortid] = label
					nameDICT[label] = shortid
					outfile.write(">"+shortid+"\n"+seqDICT[label]+"\n")
					count += 1
		else: continue
		
		#write the new tree file with name switched
		for node in intree.iternodes():
			if node.istip:
				node.label = nameDICT[node.label]
		trefile_satein = outDIR+ccID+".satein.tre"
		with open(trefile_satein,"w") as outfile:
			outfile.write(newick3.tostring(intree)+";\n")

		#call sate
		cmd = "python ~/apps/satesrc-v2.2.7-2013Feb15/sate-core/run_sate.py "
		cmd += "--input="+alnfile_satein+" --treefile="+trefile_satein
		cmd += " --aligned --datatype=DNA --num-cpus=5 --iter-limit="+str(ITERATIONS)
		print cmd
		os.system(cmd)
		
		#switch the seqids back to the full-length ones
		handle = open(outDIR+"satejob.marker001."+ccID+".satein.aln.aln","r")
		outfile = open(outDIR+ccID+".sate.aln","w")
		for record in SeqIO.parse(handle,"fasta"):
			seqid = nameDICT[str(record.id)]
			seq = str(record.seq)
			outfile.write(">"+seqid+"\n"+seq+"\n")
		
		os.system("rm "+outDIR+"satejob*")
		os.system("rm "+outDIR+"*satein*")
			
