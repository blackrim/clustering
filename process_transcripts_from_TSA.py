"""
rename the downloaded tsa fasta file to taxonID.Trinity.fasta
cd into the directory where the taxonID.Trinity.fasta file is
"""

import os,sys
import process_seq

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python process_transcripts_from_TSA.py taxonID"
		sys.exit(0)

	process_seq.run_transdecoder_blastp(taxonID=sys.argv[1],num_cores=4,stranded=False,pfam=False)
	process_seq.check_pep_coverage_redundancy(taxonID=sys.argv[1],hitID="Beta",min_pident=60.0,log=True)
