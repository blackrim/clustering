"""
Given a dir with all the codemlDIR
Move all the good codemlDIRs to outDIR
"""

import sys,os
from Bio.Phylo.PAML import codeml
from Bio.Phylo.PAML.chi2 import cdf_chi2

DIR_ENDING = ".codemlDIR"
FILE_ENDING = ".codemlout"

if __name__ =="__main__":
	if len(sys.argv) != 4:
		print "usage: codeml_reader.py DIR(with all the codemlDIR in it) outDIR outfile"
		sys.exit()
	
	DIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	out = sys.argv[3]
	outfile = open(out,"w")
	outfile.write("name\tomega\n")
	for i in os.listdir(DIR):
		if i[-len(DIR_ENDING):] != DIR_ENDING: continue
		name = i.replace(DIR_ENDING,"")
		print name
		cml = codeml.Codeml()
		cml.out_file= DIR+i+"/"+name+".codemlout"
		cml.working_dir = DIR+i+"/"			
		results = codeml.read(cml.out_file)
		a = results.get("NSsites")
		if a != None:
			b0 = a.get(0)
			if b0!=None:
				outfile.write(name+"\t"+str((b0.get("parameters")).get('omega'))+"\n")
				#move this checked results out
				#many codeml results will be empty
				os.system("mv "+DIR+i+" "+outDIR+i)
	outfile.close()
