import newick3,phylo3,os,sys
from tree_utils import *

def get_121_for_ASTRAL(indir,min_taxa,outdir,min_bootstrap=80.0):
	if indir[-1] != "/": indir += "/"
	if outdir[-1] != "/": outdir += "/"
	min_taxa = int(min_taxa)
	min_bootstrap = float(min_bootstrap)
	infile_count, outfile_count = 0,0
	print "Filter one-to-one homologs with average bootstrap of at least",\
		min_bootstrap
	outfile1 = open(outdir+"all_ML.trees","w")
	outfile2 = open(outdir+"all_boot.trees","w")
	for i in os.listdir(indir):
		if not i.endswith(".tre"): continue
		infile_count += 1
		with open(indir+i,"r") as infile: #only 1 tree in each file
			intree = newick3.parse(infile.readline())
		names = get_front_names(intree)
		num_tips,num_taxa = len(names),len(set(names))
		#print "number of tips:",num_tips,"number of taxa:",num_taxa
		if num_tips == num_taxa and num_taxa >= min_taxa:
			if min_bootstrap > 0.0 and not pass_boot_filter(intree,min_bootstrap):
				continue
			with open(indir+i,"r") as infile:
				for line in infile:
					if len(line.strip()) > 0:
						tree = newick3.parse(line)
						for node in tree.iternodes():
							#node.length = None # remove branch length
							if node.istip: # replace with taxon id
								node.label = get_name(node.label)
							else:
								node.label = None # remove bootstrap numbers
								# somehow ASTRAL can't take the node labels
						outfile1.write(newick3.tostring(tree)+");\n")
						# It's odd that the parser requires an additional ")"
			
			bootfile = i.replace(".tre",".trees")
			outfile2.write(bootfile+"\n")
			with open(indir+bootfile,"r") as infile:
				with open(outdir+bootfile,"w") as outfile3:
					for line in infile:
						if len(line.strip()) > 0:
							tree = newick3.parse(line)
							for node in tree.iternodes():
								#node.length = None # remove branch length
								if node.istip: # replace with taxon id
									node.label = get_name(node.label)
								#else: node.label = None
							outfile3.write(newick3.tostring(tree)+");\n")
			outfile_count += 1
	outfile1.close()
	outfile2.close()
	assert infile_count > 0,\
		"No file ends with "+tree_file_ending+" was found in "+indir
	print infile_count,"files read,",outfile_count,"written to",outdir

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python filter_1to1_orthologs_for_ASTRAL.py homoTreeDIR minimal_taxa ASTRAL_DIR"
		sys.exit(0)
	
	indir,min_taxa,outdir = sys.argv[1:]
	get_121_for_ASTRAL(indir,min_taxa,outdir)

