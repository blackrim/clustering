"""
Extract cds from bed and fasta
Scf00001	CoGe	CDS	875	959	.	+	.	Parent=Scf00001.g1.t1;ID=Scf00001.g1.t1.CDS1;Name=Scf00001.g1.t1.CDS1;Alias=Scf00001.g1.t1;coge_fid=366485080
Scf00001	CoGe	CDS	1106	2058	.	+	.	Parent=Scf00001.g1.t1;ID=Scf00001.g1.t1.CDS2;Name=Scf00001.g1.t1.CDS2;Alias=Scf00001.g1.t1;coge_fid=366485080

0-seqname - identifier for a sequence
1-source - program making the prediction, or if it comes from public database annotation, or is experimentally verified, etc.
2-feature
3-start - Sequence numbering starts at 1
4-end
6-strand - Defines the strand - either '+' or '-'.
8-attribute
"""
import os,sys
from Bio import SeqIO

def ReverseComplement(seq):
    seq_dict = {'A':'T','a':'t','T':'A','t':'a','G':'C','g':'c','C':'G','c':'g','N':'N','n':'n'}
    return "".join([seq_dict[base] for base in reversed(seq)])

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python bed_to_transcripts .gff .fa outfile"
		sys.exit(0)
	
	print "Reading in the fasta file"
	handle = open(sys.argv[2],"r")
	genomeDICT = {} #key is contigID, value is seq
	for record in SeqIO.parse(handle,"fasta"):
		genomeDICT[str(record.id).replace("lcl|","")] = str(record.seq)
	handle.close
	
	print "Parsing the gff file"
	infile = open(sys.argv[1],"r")
	cdsDICT = {} #key is name, value is the combined cds
	revcomp = [] #store the cds that need to be flipped
	for line in infile:
		if len(line) < 3 or line[0] == "#": continue
		spls = line.strip().split("\t")
		if len(spls) != 9:
			print "Error in parsing bed file"
			print line
			sys.exit(0)
		if spls[2] != "CDS": continue #only take cds

		genome = genomeDICT[spls[0]]
		name = ((spls[8].split(";")[0]).replace("Parent=","")).replace(".","_")
		strand = spls[6]
		start,end = int(spls[3]),int(spls[4])
		print name,strand,start,end
		if name not in cdsDICT:
			cdsDICT[name] = ""
		cdsDICT[name] += genome[start-1:end]
		if strand == "-": revcomp.append(name)
	infile.close()
	print revcomp
	
	print "Writing output fasta"
	outfile = open(sys.argv[3],"w")
	for name in cdsDICT:
		cds = cdsDICT[name]
		if name in revcomp:
			cds = ReverseComplement(cds)
		outfile.write(">"+name+"\n"+cds+"\n")
	outfile.close()
