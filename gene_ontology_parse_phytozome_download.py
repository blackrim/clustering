"""
Parse the phytozome download annotation info.text files
concatenate all 27 files to the list file
"""

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python gene_ontology_parse_phytozome_download.py.py list"
		sys.exit(0)
	
	#get the relationship between seq and GO
	infile = open(sys.argv[1],"r")
	outfile1 = open(sys.argv[1]+".pep","w")
	outfile2 = open(sys.argv[1]+".dna","w")
	for line in infile:
		if "GO:" not in line: continue #only look at genes with GO
		spls = (line.strip()).split("\t")
		#print spls
		#0-peptide id, 3 is transcript id
		pepid = spls[0] #peptide id
		trid1 = spls[2] #transcript id
		trid2 = spls[3] #transcript id
		for i in spls:
			if "GO:" in i:
				outfile1.write(pepid+"\t"+i+"\n")
				outfile2.write(trid1+"\t"+i+"\n")
				if trid2 != trid1:
					outfile2.write(trid2+"\t"+i+"\n")
	infile.close()
	outfile1.close()
	outfile2.close()
