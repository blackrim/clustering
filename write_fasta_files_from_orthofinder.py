"""
Read the concatenated fasta file
write individual fasta files for each cluster
"""

import sys,os
from seq import read_fasta_file
from tree_utils import get_name

def orthofinder_to_fasta(all_fasta,orthofinder_outfile,minimal_taxa,outdir):
	print "Reading orthofinder output file"
	clusterDICT = {} #key is seqID, value is clusterID
	minimal_taxa = int(minimal_taxa)
	count = 0
	with open(orthofinder_outfile,"r") as infile:
		for line in infile:
			# line looks like
			# OG0025091: BP@Contig15146_2884_3062_3_minus AI@Contig6575_1493_1722_3_minus
			spls = line.strip().split(':')
			if len(spls) != 2: continue
			clusterID = spls[0]
			seqIDs = spls[1].strip().split(" ")
			num_taxa = len(set(get_name(i) for i in seqIDs))
			if num_taxa >= minimal_taxa:
				print clusterID,num_taxa
				count += 1
				for seqID in seqIDs:
					clusterDICT[seqID] = str(clusterID)
	print count,"clusters read"
					
	print "Reading the fasta file",all_fasta
	for s in read_fasta_file(all_fasta):
		try:
			clusterID = clusterDICT[s.name]
			with open(outdir+clusterID+".fa","a") as outfile:
				outfile.write(">"+s.name+"\n"+s.seq+"\n")
		except: pass # Seqs that did not go in a cluster with enough taxa
	print count,"clusters with at least",minimal_taxa,"taxa written to",outdir

if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: write_fasta_files_from_orthofinder.py all_fasta orthofinder_txt_outfile minimal_taxa outDIR"
		sys.exit()
	
	orthofinder_to_fasta(all_fasta=sys.argv[1],orthofinder_outfile=sys.argv[2],\
				 minimal_taxa=int(sys.argv[3]),outdir=sys.argv[4])
	
