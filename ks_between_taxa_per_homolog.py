"""
make sure that cd-hit is in the path and is executable
make sure that synonymous_calc.py and bin from
the kaks-calculator https://code.google.com/p/kaks-calculator/
are in the current directory.

Create a tabular file that each line contains
code	taxon_name
separated by tab
"""

import sys,os
from Bio import SeqIO
import newick3,phylo3
import tree_utils
from collections import Counter

taxa_to_ignore = ["TelSFB"]
# ignoring TelSFB for now because I re-did the assembly and there's no match now
# fix this in the next round

max_ks_to_plot = 1.0


def get_output_name(taxon_name):
	"""reformat taxon name for output.
	Modify it according to the output format needed"""
	spls = taxon_name.split("_")
	return spls[1]+" "+spls[2].split("=")[0] # genus species

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python ks_between_taxa.py rooted_cladeDIR all_cds all_pep num_cores taxon_name_table"
		sys.exit(0)
	
	indir,all_cds,all_pep,num_cores,taxon_table = sys.argv[1:]
	if indir[-1] != "/": indir += "/"
	outdir = "./"
	"""
	outfile = open("seq_pairs","w")
	for i in os.listdir(indir):
		# read in the extracted, rooted clade
		with open(indir+i,"r") as infile:
			intree = newick3.parse(infile.readline())
		
		# remove highly repeated taxa
		labels = tree_utils.get_front_labels(intree)
		taxa = tree_utils.get_front_names(intree) # with dups
		taxon_set = set(taxa) # no dup
		ignore = [] #taxa that repeated >10 times. Ignore it
		for taxon in taxon_set:
			count = taxa.count(taxon)
			if count > 10:
				ignore.append(taxon)
				print taxon,"repeated",count,"teims, ignore in Ks calculation"
		if len(taxon_set) - len(ignore) < 2:
			continue # 0 or 1 taxa left
		if len(ignore) > 0: print i, ignore
		
		# get all-by-all unique seq pairs from each clade
		for label1 in labels:
			if label1 in ignore: continue
			if tree_utils.get_name(label1) in taxa_to_ignore: continue
			for label2 in labels:
				if label2 in ignore or label1 <= label2: continue
				if tree_utils.get_name(label2) in taxa_to_ignore: continue
				outfile.write(label1+"\t"+label2+"\t"+i+"\n")
	outfile.close()
	
	pairs = {} # dictiionary of tuples of taxon pairs: seqid pairs
	with open("seq_pairs","r") as infile:
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) >= 2:
				seqid1,seqid2 = spls[0],spls[1]
				taxon1,taxon2 = tree_utils.get_name(seqid1),tree_utils.get_name(seqid2)
				assert seqid1 >= seqid2
				if taxon1 < taxon2: taxon1,taxon2 = taxon2, taxon1
				if (taxon1,taxon2) not in pairs:
					pairs[(taxon1,taxon2)] = []
				pairs[(taxon1,taxon2)].append((seqid1,seqid2))

	# write all the cds pairs
	DICT = {} #key is seqid, value is seq
	with open(all_cds,"r") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			DICT[str(record.id)] = str(record.seq)
	for taxon_pair in pairs:
		with open(taxon_pair[0]+"-"+taxon_pair[1]+".cds.pairs.fa","w") as outfile:
			for seqid_pair in pairs[taxon_pair]:
				outfile.write(">"+seqid_pair[0]+"\n"+DICT[seqid_pair[0]]+"\n")
				outfile.write(">"+seqid_pair[1]+"\n"+DICT[seqid_pair[1]]+"\n\n")
	# write all the pep pairs
	DICT = {} #key is seqid, value is seq
	with open(all_pep,"r") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			DICT[str(record.id)] = str(record.seq)
	for taxon_pair in pairs:
		with open(taxon_pair[0]+"-"+taxon_pair[1]+".pep.pairs.fa","w") as outfile:
			for seqid_pair in pairs[taxon_pair]:
				outfile.write(">"+seqid_pair[0]+"\n"+DICT[seqid_pair[0]]+"\n")
				outfile.write(">"+seqid_pair[1]+"\n"+DICT[seqid_pair[1]]+"\n\n")
	print len(pairs),"sequence pairs wirtten"
	
	
	for pep in os.listdir(outdir):
		if not pep.endswith(".pep.pairs.fa"): continue
		cds = pep.replace("pep","cds")
		taxonid_pair = pep.replace(".pep.pairs.fa","")
		cmd = "python synonymous_calc.py "+pep+" "+cds+" >"+taxonid_pair+".ks"
		print cmd
		os.system(cmd)
		#os.system("python report_ks.py "+taxon+".ks")

		# parse the ks output
		first = True
		infile = open(taxonid_pair+".ks","r")
		outfile1 = open(taxonid_pair+".ks.yn","w")
		outfile2 = open(taxonid_pair+".ks.ng","w")
		for line in infile:
			if first:
				first = False
				continue
			spls = line.strip().split(",")
			# 0-name,1-dS-yn,2-dN-yn,3-dS-ng,4-dN-ng
			yn,ng = float(spls[1]),float(spls[3])
			if yn >= 0.0 and yn <= max_ks_to_plot: # between sp 0 is ok
				outfile1.write(spls[1]+"\n")
			if ng >= 0.0 and ng <= max_ks_to_plot:
				outfile2.write(spls[3]+"\n") 
		infile.close()
		outfile1.close()
		outfile2.close()"""

	# output looks like taxonid_pair.ks.ng and taxonid_pair.ks.yn
	# write the scripts to be run in R to plot the output
	DICT = {} # key is taxon, value is long species name
	with open(taxon_table, "rU") as infile:
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) > 1:
				DICT[spls[0]] = spls[1]
	
	out = ""
	for name in os.listdir(outdir):
		#if name.endswith(".ng") or name.endswith(".yn"):
		if name.endswith(".ng"):
			taxon1,taxon2 = (name.split(".")[0]).split("-")
			out += "pdf=pdf(file='"+name+".pdf',width=7,height=7)\n"
			out += "a<-read.table('"+name+"')\n"
			out += "hist(a[,1],breaks=50,col='grey',xlab='',ylab='',main='"
			out += get_output_name(DICT[taxon1])+" vs. "+get_output_name(DICT[taxon2])+"',"
			out += "xlim=c(0,"+str(max_ks_to_plot)+"),axes=FALSE)\n"
			out += "axis(1,pos=0)\naxis(2,pos=0)\ndev.off()\n\n"
	with open("R_scripts_ks_plots","w") as f: f.write(out)
	
	os.system("Rscript R_scripts_ks_plots")
