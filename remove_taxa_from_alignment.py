from Bio import SeqIO
import sys,os

remove = ["JBGU","HTDC","QAIR","HSXO"]
file_end = ".fa"

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: 100taxa_to_96taxa.py 100DIR 96DIR"
		sys.exit()
	
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	
	for i in os.listdir(inDIR):
		if i[-len(file_end):] == file_end:
			handle = open(inDIR+i)
			outfile = open(outDIR+i,"w")
			for record in SeqIO.parse(handle,"fasta"):
				seqid = record.id
				seqname = seqid.replace("_R_","")[:4]
				if seqname[:4] not in remove:
					seq = record.seq
					outfile.write(">"+str(seqid)+"\n"+str(seq)+"\n")
			handle.close()
